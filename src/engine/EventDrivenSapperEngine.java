package engine;

import engine.boards.*;
import engine.emitters.SapperEventEmitter;

public class EventDrivenSapperEngine implements SapperEngine {
    private final SapperEventEmitter emitter;
    private final ImmutableBoard solutionBoard;
    private final MutableBoard userBoard;
    private final BoardNeighborPositionSelector selector;
    private int leftFields;

    public EventDrivenSapperEngine(ImmutableBoard solution, SapperEventEmitter emitter) {
        this.solutionBoard = solution;
        this.emitter = emitter;
        userBoard = new ArrayMutableBoard(solution.getRowCount(), solution.getColumnCount());
        leftFields = solution.getRowCount() * solution.getColumnCount() - solution.countSymbol(FieldSymbol.BOMB);
        selector = new BoardNeighborPositionSelector(solutionBoard);
    }

    @Override
    public void useField(int rowIndex, int columnIndex) {
        if (userBoard.getFieldSymbol(rowIndex, columnIndex) == null) {
            FieldSymbol symbol = solutionBoard.getFieldSymbol(rowIndex, columnIndex);

            if (symbol.isNumeric()) {
                --leftFields;
            }

            userBoard.setFieldSymbol(rowIndex, columnIndex, symbol);
            emitter.showField(rowIndex, columnIndex, symbol);

            if (symbol == FieldSymbol.NUMBER_0) {
                showNeighboringFields(rowIndex, columnIndex);
            }
            else if (symbol == FieldSymbol.BOMB) {
                emitter.failure();
            }
            else if (leftFields == 0) {
                emitter.success();
            }
        }
    }

    private void showNeighboringFields(int rowIndex, int columnIndex) {
        for (BoardPosition position : selector.selectFor(rowIndex, columnIndex)) {
            useField(position.getRowIndex(), position.getColumnIndex());
        }
    }

    @Override
    public int getRowCount() {
        return solutionBoard.getRowCount();
    }

    @Override
    public int getColumnCount() {
        return solutionBoard.getColumnCount();
    }
}
