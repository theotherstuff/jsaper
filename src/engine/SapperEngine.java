package engine;

public interface SapperEngine {
    void useField(int rowIndex, int columnIndex);
    int getRowCount();
    int getColumnCount();
}
