package engine.boards;


public final class ArrayImmutableBoard implements ImmutableBoard {
    private final MutableBoard board;

    public ArrayImmutableBoard(FieldSymbol[][] fields) {
        board = new ArrayMutableBoard(fields);
    }

    @Override
    public int getColumnCount() {
        return board.getColumnCount();
    }

    @Override
    public int getRowCount() {
        return board.getRowCount();
    }

    @Override
    public int countSymbol(FieldSymbol symbol) {
        return board.countSymbol(symbol);
    }

    @Override
    public boolean containsPosition(BoardPosition position) {
        return board.containsPosition(position);
    }

    @Override
    public FieldSymbol getFieldSymbol(int rowIndex, int columnIndex) {
        return board.getFieldSymbol(rowIndex, columnIndex);
    }
}
