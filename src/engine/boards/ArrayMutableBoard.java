package engine.boards;

public class ArrayMutableBoard implements MutableBoard {
    private final FieldSymbol[][] fields;
    private final int rowCount;
    private final int columnCount;

    public ArrayMutableBoard(int rowCount, int columnCount) {
        fields = new FieldSymbol[rowCount][columnCount];
        this.rowCount = rowCount;
        this.columnCount = columnCount;
    }

    public ArrayMutableBoard(FieldSymbol[][] fields) {
        this.fields = fields.clone();
        rowCount = fields.length;
        columnCount = fields[0].length;
    }

    @Override
    public FieldSymbol getFieldSymbol(int rowIndex, int columnIndex) {
        return fields[rowIndex][columnIndex];
    }

    @Override
    public void setFieldSymbol(int rowIndex, int columnIndex, FieldSymbol symbol) {
        fields[rowIndex][columnIndex] = symbol;
    }


    @Override
    public int getColumnCount() {
        return columnCount;
    }

    @Override
    public int getRowCount() {
        return rowCount;
    }

    @Override
    public int countSymbol(FieldSymbol symbol) {
        int count = 0;

        for (FieldSymbol[] row : fields) {
            for (FieldSymbol currentSymbol : row) {
                if (currentSymbol == symbol) {
                    ++count;
                }
            }
        }

        return count;
    }

    @Override
    public boolean containsPosition(BoardPosition position) {
        return (0 <= position.getRowIndex() && position.getRowIndex() < rowCount) &&
               (0 <= position.getColumnIndex() && position.getColumnIndex() < columnCount);
    }
}
