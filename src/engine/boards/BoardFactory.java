package engine.boards;

public interface BoardFactory {

    public ImmutableBoard create();
}
