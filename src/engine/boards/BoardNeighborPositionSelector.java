package engine.boards;

import java.util.ArrayList;
import java.util.List;


public class BoardNeighborPositionSelector {
    private ImmutableBoard board;

    public BoardNeighborPositionSelector(ImmutableBoard board) {
        this.board = board;
    }

    public List<BoardPosition> selectFor(int rowIndex, int columnIndex) {
        List<BoardPosition> validPositions = new ArrayList<BoardPosition>();
        for (BoardPosition position : createAllNeighborPositionsOf(rowIndex, columnIndex)) {
            if (board.containsPosition(position)) {
                validPositions.add(position);
            }
        }
        return validPositions;
    }

    private List<BoardPosition> createAllNeighborPositionsOf(int rowIndex, int columnIndex) {
        List<BoardPosition> positions = new ArrayList<BoardPosition>();
        positions.add(new BoardPosition(rowIndex - 1, columnIndex - 1));
        positions.add(new BoardPosition(rowIndex - 1, columnIndex));
        positions.add(new BoardPosition(rowIndex - 1, columnIndex + 1));
        positions.add(new BoardPosition(rowIndex, columnIndex - 1));
        positions.add(new BoardPosition(rowIndex, columnIndex + 1));
        positions.add(new BoardPosition(rowIndex + 1, columnIndex - 1));
        positions.add(new BoardPosition(rowIndex + 1, columnIndex));
        positions.add(new BoardPosition(rowIndex + 1, columnIndex + 1));
        return positions;
    }

}
