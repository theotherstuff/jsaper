package engine.boards;

public final class BoardPosition {

    private final int columnIndex;
    private final int rowIndex;

    public BoardPosition(int rowIndex, int columnIndex) {
        this.rowIndex = rowIndex;
        this.columnIndex = columnIndex;
    }

    public int getColumnIndex() {
        return columnIndex;
    }

    public int getRowIndex() {
        return rowIndex;
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other instanceof BoardPosition) {
            BoardPosition position = (BoardPosition) other;
            return position.rowIndex == rowIndex && position.columnIndex == columnIndex;
        }
        return false;
    }

    @Override
    public int hashCode() {
        // http://stackoverflow.com/a/11742634
        return rowIndex * 31 + columnIndex;
    }
}
