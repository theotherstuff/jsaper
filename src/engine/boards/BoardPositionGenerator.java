package engine.boards;

import java.util.Set;

public interface BoardPositionGenerator {

    Set<BoardPosition> generateUniquePositions(int positionCount, int rowCount, int columnCount);

}
