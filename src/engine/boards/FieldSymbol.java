package engine.boards;

public enum FieldSymbol {
    NUMBER_0,
    NUMBER_1,
    NUMBER_2,
    NUMBER_3,
    NUMBER_4,
    NUMBER_5,
    NUMBER_6,
    NUMBER_7,
    NUMBER_8,
    BOMB;

    public FieldSymbol nextSymbol() {
        switch (this) {
            case NUMBER_0:
                return NUMBER_1;
            case NUMBER_1:
                return NUMBER_2;
            case NUMBER_2:
                return NUMBER_3;
            case NUMBER_3:
                return NUMBER_4;
            case NUMBER_4:
                return NUMBER_5;
            case NUMBER_5:
                return NUMBER_6;
            case NUMBER_6:
                return NUMBER_7;
            case NUMBER_7:
                return NUMBER_8;
            default:
                throw new IllegalArgumentException();
        }
    }


    public boolean isNumeric() {
        return this != BOMB;
    }
}
