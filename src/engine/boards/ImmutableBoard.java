package engine.boards;

public interface ImmutableBoard {
    int getColumnCount();

    int getRowCount();

    int countSymbol(FieldSymbol symbol);

    boolean containsPosition(BoardPosition position);

    FieldSymbol getFieldSymbol(int rowIndex, int columnIndex);
}
