package engine.boards;

public interface MutableBoard extends ImmutableBoard {
    void setFieldSymbol(int rowIndex, int columnIndex, FieldSymbol symbol);
}
