package engine.boards;

import java.util.List;


public interface NeighborPositionSelector {
    public List<BoardPosition> selectFor(int rowIndex, int columnIndex);
}
