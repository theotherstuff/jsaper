package engine.boards;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class RandomBoardPositionGenerator implements BoardPositionGenerator {
    private final Random random;

    public RandomBoardPositionGenerator(Random random) {
        this.random = random;
    }

    @Override
    public Set<BoardPosition> generateUniquePositions(int positionCount, int rowCount, int columnCount) {
        Set<BoardPosition> positions = new HashSet<BoardPosition>();
        while (positions.size() != positionCount) {
            positions.add(generatePosition(rowCount, columnCount));
        }
        return positions;
    }

    private BoardPosition generatePosition(int rowCount, int columnCount) {
        return new BoardPosition(random.nextInt(rowCount), random.nextInt(columnCount));
    }
}