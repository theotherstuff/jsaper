package engine.boards;

import java.util.Set;

public class RandomSapperBoardFactory implements BoardFactory {
    private int rowCount;
    private int columnCount;
    private int bombCount;
    private final BoardPositionGenerator bombPositionGenerator;

    public RandomSapperBoardFactory(BoardPositionGenerator bombPositionGenerator, int rows, int columns, int bombs) {
        this.bombPositionGenerator = bombPositionGenerator;
        rowCount = rows;
        columnCount = columns;
        bombCount = bombs;
    }

    public ImmutableBoard create() {
        FieldSymbol[][] fields = createFields();
        Set<BoardPosition> bombPositions = createBombPositions();
        insertBombs(fields, bombPositions);
        calculateNumericFields(fields);
        return new ArrayImmutableBoard(fields);
    }

    private void insertBombs(FieldSymbol[][] fields, Set<BoardPosition> bombPositions) {
        for (BoardPosition position : bombPositions) {
            fields[position.getRowIndex()][position.getColumnIndex()] = FieldSymbol.BOMB;
        }
    }

    private FieldSymbol[][] createFields() {
        FieldSymbol[][] fields = new FieldSymbol[rowCount][columnCount];
        for (int rowIndex = 0; rowIndex < fields.length; ++rowIndex) {
            for (int columnIndex = 0; columnIndex < fields[rowIndex].length; ++columnIndex) {
                fields[rowIndex][columnIndex] = FieldSymbol.NUMBER_0;
            }
        }
        return fields;
    }

    private void calculateNumericFields(FieldSymbol[][] fields) {
        for (int rowIndex = 0; rowIndex < rowCount; ++rowIndex) {
            for (int columnIndex = 0; columnIndex < columnCount; ++columnIndex) {
                if (fields[rowIndex][columnIndex] == FieldSymbol.BOMB) {
                    incrementNumericFields(fields, rowIndex, columnIndex);
                }
            }
        }
    }

    private void incrementNumericFields(FieldSymbol[][] fields, int rowIndex, int columnIndex) {
        BoardNeighborPositionSelector selector = new BoardNeighborPositionSelector(new ArrayImmutableBoard(fields));
        for (BoardPosition position : selector.selectFor(rowIndex, columnIndex)) {
            FieldSymbol symbol = fields[position.getRowIndex()][position.getColumnIndex()];
            if (symbol != FieldSymbol.BOMB) {
                fields[position.getRowIndex()][position.getColumnIndex()] = symbol.nextSymbol();
            }
        }
    }

    private Set<BoardPosition> createBombPositions() {
        return bombPositionGenerator.generateUniquePositions(bombCount, rowCount, columnCount);
    }
}