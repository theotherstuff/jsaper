package engine.emitters;

import engine.boards.FieldSymbol;

public interface SapperEventEmitter {

    void success();

    void failure();

    void showField(int rowIndex, int columnIndex, FieldSymbol symbol);
}