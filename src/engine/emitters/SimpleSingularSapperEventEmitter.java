package engine.emitters;


import engine.boards.FieldSymbol;
import engine.listeners.FailureListener;
import engine.listeners.ShowFieldListener;
import engine.listeners.SuccessListener;

public class SimpleSingularSapperEventEmitter implements SingularSapperEventEmitter {
    private SuccessListener successListener;
    private FailureListener failureListener;
    private ShowFieldListener showFieldListener;

    @Override
    public void setSuccessListener(SuccessListener listener) {
        successListener = listener;
    }

    @Override
    public void setFailureListener(FailureListener listener) {
        failureListener = listener;
    }

    @Override
    public void setShowFieldListener(ShowFieldListener listener) {
        showFieldListener = listener;
    }

    @Override
    public void success() {
        successListener.onSuccess();
    }

    @Override
    public void failure() {
        failureListener.onFailure();
    }

    @Override
    public void showField(int rowIndex, int columnIndex, FieldSymbol symbol) {
        showFieldListener.onShowField(rowIndex, columnIndex, symbol);
    }
}
