package engine.emitters;

import engine.listeners.FailureListener;
import engine.listeners.ShowFieldListener;
import engine.listeners.SuccessListener;

public interface SingularSapperEventEmitter extends SapperEventEmitter{

    void setSuccessListener(SuccessListener listener);

    void setFailureListener(FailureListener listener);

    void setShowFieldListener(ShowFieldListener listener);

}
