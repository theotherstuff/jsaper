package engine.listeners;

public interface FailureListener {
    void onFailure();
}
