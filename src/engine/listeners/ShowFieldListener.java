package engine.listeners;

import engine.boards.FieldSymbol;

public interface ShowFieldListener {
    void onShowField(int rowIndex, int columnIndex, FieldSymbol symbol);
}
