package tui;

import engine.boards.FieldSymbol;
import engine.boards.ImmutableBoard;

public class BoardPrinter {

    public void print(ImmutableBoard board) {
        printHeaders(board);
        for (int rowIndex = 0; rowIndex < board.getRowCount(); ++rowIndex) {
            System.out.print(rowIndex + "#|");
            for (int columnIndex = 0; columnIndex < board.getColumnCount(); ++columnIndex) {
                System.out.print(translateSymbolToChar(board.getFieldSymbol(rowIndex, columnIndex)) + "|");
            }
            System.out.println();
        }
    }

    private void printHeaders(ImmutableBoard board) {
        System.out.print("  |");
        for (int i = 0; i < board.getColumnCount(); ++i) {
            System.out.print(i + "|");
        }
        System.out.println();
        System.out.println("######################");
    }

    private char translateSymbolToChar(FieldSymbol symbol) {
        if (symbol == null) {
            return '_';
        }

        switch (symbol) {
            case NUMBER_0:
                return '0';
            case NUMBER_1:
                return '1';
            case NUMBER_2:
                return '2';
            case NUMBER_3:
                return '3';
            case NUMBER_4:
                return '4';
            case NUMBER_5:
                return '5';
            case NUMBER_6:
                return '6';
            case NUMBER_7:
                return '7';
            case NUMBER_8:
                return '8';
            case BOMB:
                return '@';
            default:
                throw new IllegalArgumentException();
        }
    }

}
