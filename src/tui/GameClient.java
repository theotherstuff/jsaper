package tui;

import engine.SapperEngine;
import engine.boards.ArrayMutableBoard;
import engine.boards.BoardPosition;
import engine.boards.FieldSymbol;
import engine.boards.MutableBoard;
import engine.listeners.FailureListener;
import engine.listeners.ShowFieldListener;
import engine.listeners.SuccessListener;

public class GameClient implements FailureListener, ShowFieldListener, SuccessListener {
    private final GameInput gameInput;
    private final GameOutput gameOutput;
    private SapperEngine engine;
    private GameStatus status = GameStatus.PENDING;
    private MutableBoard board;

    public GameClient(SapperEngine engine, GameInput gameInput, GameOutput gameOutput) {
        this.engine = engine;
        this.board = new ArrayMutableBoard(engine.getRowCount(), engine.getColumnCount());
        this.gameInput = gameInput;
        this.gameOutput = gameOutput;
    }

    @Override
    public void onFailure() {
        status = GameStatus.FAILURE;
    }

    @Override
    public void onShowField(int rowIndex, int columnIndex, FieldSymbol symbol) {
        board.setFieldSymbol(rowIndex, columnIndex, symbol);
    }

    @Override
    public void onSuccess() {
        status = GameStatus.SUCCESS;
    }

    private boolean isRunning() {
        return status == GameStatus.PENDING;
    }

    private boolean isSuccess() {
        return status == GameStatus.SUCCESS;
    }

    private boolean isFailure() {
        return status == GameStatus.FAILURE;
    }

    public void run() {
        gameOutput.printTitle("SAPER");

        while (isRunning()) {
            gameOutput.printBoard(board);
            gameOutput.printPrompt();
            BoardPosition position = gameInput.getPosition();
            engine.useField(position.getRowIndex(), position.getColumnIndex());
        }

        if (isSuccess()) {
            gameOutput.printCongratulations();
        }
        else {
            gameOutput.printGameOver();
        }
    }
}
