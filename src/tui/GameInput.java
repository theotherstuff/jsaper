package tui;

import engine.boards.BoardPosition;

import java.util.Scanner;

public class GameInput {
    private final Scanner scanner;
    private final int rowCount;
    private final int columnCount;

    public GameInput(Scanner scanner, int rowCount, int columnCount) {
        this.rowCount = rowCount;
        this.columnCount = columnCount;
        this.scanner = scanner;
    }

    public BoardPosition getPosition() {
        return new BoardPosition(getIndex(rowCount), getIndex(columnCount));
    }

    private int getIndex(int max) {
        while (true) {
            while (scanner.hasNextInt()) {
                int value = scanner.nextInt();
                if (0 <= value && value < max) {
                    return value;
                }
            }
            scanner.next(); // ignore
        }
    }


}
