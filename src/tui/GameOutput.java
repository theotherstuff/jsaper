package tui;

import engine.boards.MutableBoard;


public class GameOutput {
    private final BoardPrinter boardPrinter;


    public GameOutput(BoardPrinter boardPrinter) {
        this.boardPrinter = boardPrinter;
    }

    public void printTitle(String title) {
        System.out.println(String.format("               :: %s ::", title));
    }

    public void printBoard(MutableBoard board) {
        boardPrinter.print(board);
    }

    public void printPrompt() {
        System.out.print("> ");
    }

    public void printCongratulations() {
        System.out.println("!!!!!!!!!!!!!!!!");
        System.out.println("CONGRATULATION!");
        System.out.println("!!!!!!!!!!!!!!!!");
    }

    public void printGameOver() {
        System.out.println("!!!!!!!!!!!!!!!!");
        System.out.println("GAME  OVER");
        System.out.println("!!!!!!!!!!!!!!!!");
    }
}
