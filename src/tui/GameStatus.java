package tui;

public enum GameStatus {
    PENDING,
    SUCCESS,
    FAILURE
}
