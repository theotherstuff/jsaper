package tui;

import engine.EventDrivenSapperEngine;
import engine.boards.*;
import engine.emitters.SimpleSingularSapperEventEmitter;

import java.util.Random;
import java.util.Scanner;

public class TUISapperApplication {

    void run() {
        SimpleSingularSapperEventEmitter emitter = new SimpleSingularSapperEventEmitter();
        BoardPositionGenerator bombPositionGenerator = new RandomBoardPositionGenerator(new Random());
        BoardFactory boardFactory = new RandomSapperBoardFactory(bombPositionGenerator, 10, 10, 10);
        ImmutableBoard solutionBoard = boardFactory.create();
        EventDrivenSapperEngine engine = new EventDrivenSapperEngine(solutionBoard, emitter);
        GameInput input = new GameInput(new Scanner(System.in), engine.getRowCount(), engine.getColumnCount());
        GameOutput output = new GameOutput(new BoardPrinter());
        GameClient client = new GameClient(engine, input, output);
        emitter.setFailureListener(client);
        emitter.setShowFieldListener(client);
        emitter.setSuccessListener(client);
        client.run();
    }


    public static void main(String[] args) {
        TUISapperApplication app = new TUISapperApplication();
        app.run();
    }

}
