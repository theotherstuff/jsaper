package boards;

import engine.boards.ArrayMutableBoard;
import engine.boards.FieldSymbol;
import engine.boards.MutableBoard;
import org.junit.Test;

import static junit.framework.Assert.*;

public class TestArrayMutableBoard {

    @Test
    public void testRowCount(){
        MutableBoard board = new ArrayMutableBoard(3, 10);
        assertEquals(3, board.getRowCount());
    }

    @Test
    public void testColumnCount(){
        MutableBoard board = new ArrayMutableBoard(3, 10);
        assertEquals(10, board.getColumnCount());
    }

    @Test
    public void testRowCountWhenCreatedByArray(){
        MutableBoard board = new ArrayMutableBoard(new FieldSymbol[4][6]);
        assertEquals(4, board.getRowCount());
    }

    @Test
    public void testColumnCountWhenCreatedByArray(){
        MutableBoard board = new ArrayMutableBoard(new FieldSymbol[4][6]);
        assertEquals(6, board.getColumnCount());
    }

    @Test
    public void testDefaultFieldValue(){
        MutableBoard board = new ArrayMutableBoard(3, 10);
        assertNull(board.getFieldSymbol(0, 0));
    }

}
