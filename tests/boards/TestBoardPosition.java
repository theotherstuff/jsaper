package boards;

import engine.boards.*;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class TestBoardPosition {

    @Test
    public void testGetX(){
        BoardPosition position = new BoardPosition(7, 9);
        assertEquals(7, position.getRowIndex());
    }

    @Test
    public void testColumnCount(){
        BoardPosition position = new BoardPosition(9, 10);
        assertEquals(10, position.getColumnIndex());
    }

    @Test
    public void testEquals(){
        BoardPosition a = new BoardPosition(9, 10);
        BoardPosition b= new BoardPosition(9, 10);
        assertEquals(a, b);
    }

    @Test
    public void testHashCode(){
        BoardPosition a = new BoardPosition(9, 10);
        BoardPosition b= new BoardPosition(9, 10);
        assertEquals(a.hashCode(), b.hashCode());
    }
}
